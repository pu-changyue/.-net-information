import http from 'http';
import fs from 'fs';

/*
    需求：根据地址栏不同的路径，读取对应路径下的index.html文件，通过响应返回给请求端，如果没有找到文件，则显示“很遗憾”

*/


let port = 3000;
let app = http.createServer((req, res) => {
    // 这个更好的理解是，这个就是一个处理请求的单元
    // 从请求中获取一些请求参数
    // 通过响应返回一些数据信息
    console.log(req.url);
    let data = fs.readFileSync('./index.html','utf-8');
    console.log(req.method, req.url);
    res.end(data);
});



app.listen(port);

console.log(`服务运行于如下地址：http://localhost:${port}`);

