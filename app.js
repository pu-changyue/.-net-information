import Koa from "koa";
import Router from "koa-router";
import bodyparser from "koa-bodyparser";
import cors from "koa2-cors";
import { Sequelize,DataTypes,Op} from "sequelize";
const app=new Koa();

let router=new Router();
let sequelize=new Sequelize('node_demo','sa','123456',{
    host:'localhost',
    dialect:'mssql' 
}) 

const Films=sequelize.define('Film',{
    director:{type:DataTypes.STRING},
    actor:{type:DataTypes.STRING},
    score:{type:DataTypes.INTEGER},
    flag:{type:DataTypes.STRING},
})

await Films.sync();

//获取
router.get('/Film/:id',async (ctx)=>{
    let id=ctx.params.id * 1 || 0;
    let data;
    if(id>0){
        let item=await Films.findByPk(id);
        data=item;        
    }else{
        let list=await Films.findAll();
        data=list;
    }
    ctx.body={
        code:1000,
        data:data,
        msg:'获取列表成功'
    }
})

router.get('/Film',async (ctx)=>{
     
     let keyword = ctx.request.query.keyword || '';
    keyword = keyword.trim();
    console.log(keyword);
  
    if (keyword) {
        let list = await Films.findAll({
            where: {
                [Op.or]: [
                    { director: { [Op.like]: '%' + keyword + '%' } },  
                    { actor: { [Op.like]: '%' + keyword + '%' } },  
                    { score: { [Op.like]: '%' + keyword + '%' } },  
                    { flag: { [Op.like]: '%' + keyword + '%' } },  
                ]
            }
        });
        ctx.body = {
            code: 1000,
            data: list,
            msg: '查找数据成功'
        }
    } else {
        let list = await Films.findAll();
        ctx.body = {
            code: 1000,
            data: list,
            msg: '获取所有电影成功'
        };
    }
})

//请求
router.post('/Film',async (ctx)=>{
    let obj=ctx.request.body;
    let row=await Films.create(obj);
    ctx.body={
        code:1000,
        data:row,
        msg:'新增电影成功'
    }
})

//更改
router.put('/Film/:id',async (ctx)=>{
    let id=ctx.params.id * 1 || 0;
    
    let obj=ctx.request.body;
    
    let item=await Films.findByPk(id);
    
    if(item){
       let data=await Films.update(obj,{
            where: {
                id: id               
            }
        });
        ctx.body={
            code:1000,
            data:data,
            msg:'修改成功'
        }
    }else{
        ctx.body={
            code:4000,
            data:null,
            msg:'找不到'
        }
    }
})

//删除
router.delete('/Film/:id',async (ctx)=>{
    let id=ctx.params.id || 0;
    let row=Films.findByPk(id);    
    if(row){
        Films.destroy({
            where:{
                id:id
            }           
        })        
        ctx.body={
            code:2000,
            
            msg:'删除成功'
        }
    }else{
        ctx.body={
            code:4000,
            
            msg:'找不到你删除的对象'
        }
    }
})


app.use(cors());
app.use(bodyparser());
app.use(router.routes());


let port=3000;

app.listen(port);

console.log(`Server is running at:http://localhost:${port}`)

