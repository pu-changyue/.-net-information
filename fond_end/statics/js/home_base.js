$(function(){
    axios.get(`http://localhost:3000/Film`).then(res=>{
        let datas=res.data;
        tableUpdate(datas.data)
    })
})

function tableUpdate(content){   
    content.forEach(item=>{
        let tbs=$('.tb');
        let html=`
        <tr class=trr${item.id}>
            <td>${item.id}</td>
            <td>${item.director}</td>
            <td>${item.actor}</td>
            <td>${item.score}</td>
            <td>${item.flag}</td>
            <td>
            <input type="button" value="删除" onclick="btnDel(${item.id})">
            <input type="button" value="编辑" onclick="btnEdit(${item.id})">
            </td>
        </tr>    
        `   
        tbs.append(html)    
    })
}

//删除
function btnDel(id){
    if(confirm(`是否删除id为${id}的内容?`)){
        axios.delete(`http://localhost:3000/Film/${id}`).then(res=>{
            let datas=res.data;
            let tr=$(`.trr${id}`);
            if(datas.code==2000){
                
                tr.remove();
            }
        })
    }
}

//查询
function btnQuery() {
    let keyword = $('[name="keyword"]').val();

    // console.log(keyword)

    axios.get(`http://localhost:3000/Film?keyword=${keyword}`).then(res => {
    
    let data = res.data;

    // console.log(data)

    if (data.code == 1000) {      
        let tr= $('[class^=trr]');  

        // console.log(tr)       
        
        tr.remove();
        tableUpdate(data.data);       
    }
})
}

function btnAdd() {
    location.href = './edit.html'
}

function btnEdit(id) {
    location.href = './edit.html?id=' + id;
}