
function btnSave() {
 
    
    let director = $('[name=director]').val();
    let actor = $('[name=actor]').val();
    let score = $('[name=score]').val();
    let flag = $('[name=flag]').val();

    
    let obj = {
        director, actor, score, flag
    }

    let params = getQueryString();
    let id = params.id * 1;
   
    if (id > 0) {
        axios.put(`http://localhost:3000/Film/${id}`, obj).then(res => {
            let data = res.data;
            if (data.code === 1000) {
                location.href = './home.html';
            } else {
                alert(data.msg)
            }
        })
    } else {
        axios.post(`http://localhost:3000/Film`, obj).then(res => {
            let data = res.data;
            if (data.code === 1000) {
                location.href = './home.html';
            } else {
                alert(data.msg)
            }
        })
    }
}

function btnCancel() {
    location.href = './home.html';
}


$(function () {

    let params = getQueryString();
    let id = params.id * 1;

    
    if (id > 0) {
        axios.get(`http://localhost:3000/Film/${id}`).then(res => {
            let data = res.data;            
            $('[name=director]').val(data.data.director);
            $('[name=actor]').val(data.data.actor);
            $('[name=score]').val(data.data.score);
            $('[name=flag]').val(data.data.flag);
        })
    }

})




function getQueryString(str = window.location.search) {
    const reg = /([^?&=]+)=([^&]+)/g 
    const params = {}
    str.replace(reg, (_, k, v) => params[k] = v)
    return params
}

