import fs from 'fs';

// var aa = fs.readdirSync('./');
// console.log(aa);

function listAllFile(filePath) {
    let resArr = [];
    let fileStat = fs.statSync(filePath);
    let isPathFile = fileStat.isFile();
    if (isPathFile) {
        resArr.push(filePath);
    } else {
        let arr = fs.readdirSync(filePath);
        arr.forEach(ee => {
            let lastStrIs = filePath.lastIndexOf('/') === filePath.length - 1;
            let tmpPath = lastStrIs ? filePath + ee : `${filePath}/${ee}`;
            let tmpArr = listAllFile(tmpPath);
            resArr=resArr.concat(tmpArr);
        })
    }
    return resArr;
}


let arr=listAllFile('./');
console.log(arr);